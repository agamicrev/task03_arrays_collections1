/**
 * Create a class containing two String objects and make it Comparable
 * so that the comparison only cares about the first String.
 * Fill an array and an ArrayList with objects of your class by using
 * a custom generator (eg, which generates pairs of Country-Capital).
 * Demonstrate that sorting works properly. Now make a Comparator that
 * only cares about the second String and demonstrate that sorting works
 * properly. Also perform a binary search using your Comparator.
 */
package countrycapital;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {

  public static void main(String[] args) {
    Country country = new Country("Ukraine", "Kyiv");
    List<Country> list = new ArrayList<Country>() {{
      add(new Country("Finland", "Helsinki"));
      add(new Country("Belgium", "Brussels"));
      add(new Country("Poland", "Warshava"));
      add(new Country("Bulgaria", "Sofia"));
      add(new Country("Latvia", "Riga"));
      add(new Country("France", "Paris"));
      add(new Country("China", "Beijing"));
      add(new Country("Canada", "Toronto"));
      add(new Country("Georgia", "Tbilisi"));
      add(country);
    }};

    System.out.println("UNSORTED:");
    list.forEach(System.out::println);

    Collections.sort(list);
    System.out.println("\nSORTED BY COUNTRY:");
    list.forEach(System.out::println);

    list.sort(comparator);
    System.out.println("\nSORTED BY CAPITAL:");
    list.forEach(System.out::println);

    int seek = Collections.binarySearch(list, country, comparator);
    System.out.println("\nBinary search: " + country);
    System.out.println("Result: " + list.get(seek));
  }

  static Comparator<Country> comparator = (o1, o2) -> o1.getCapital().compareTo(o2.getCapital());
}

