package countrycapital;

public class Country implements Comparable<Country> {

  private String countryName;
  private String capital;

  public Country(String countryName, String capital) {
    this.countryName = countryName;
    this.capital = capital;
  }

  public String getCountryName() {
    return countryName;
  }

  public void setCountryName(String countryName) {
    this.countryName = countryName;
  }

  public String getCapital() {
    return capital;
  }

  public void setCapital(String capital) {
    this.capital = capital;
  }

  @Override
  public int compareTo(Country o) {
    return countryName.compareTo(o.getCountryName());
  }

  @Override
  public String toString() {
    return countryName + " - " + capital;
  }
}
