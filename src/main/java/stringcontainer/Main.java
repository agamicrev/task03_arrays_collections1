/**
 * Create a container that encapsulates an array of String, and that only
 * adds Strings and gets Strings, so that there are no casting issues during use.
 * If the internal array isn’t big enough for the next add, your container should
 * automatically resize it. In main( ), compare the performance of your container
 * with an ArrayList holding Strings.
 */
package stringcontainer;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

public class Main {

  public static void main(String[] args) {
    List<String> list = new ArrayList<>();
    long begin = System.currentTimeMillis();
    Stream.generate(UUID::randomUUID).limit(5000).forEach(i -> list.add(i.toString()));
    System.out.println("ArrayList time: " + (System.currentTimeMillis() - begin) + " ms");
    System.out.println("Size of ArrayList: " + list.size());

    CustomStringContainer customStringContainer = new CustomStringContainer();
    begin = System.currentTimeMillis();
    Stream.generate(UUID::randomUUID).limit(5000).forEach(i -> customStringContainer.add(i.toString()));
    System.out.println("CustomStringContainer time: " + (System.currentTimeMillis() - begin) + " ms");
    System.out.println("Size of CustomStringContainer: " + customStringContainer.size());
  }
}
