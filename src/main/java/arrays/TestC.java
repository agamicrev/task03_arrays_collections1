package arrays;

/**
 * C. Find in an array all series of identical items that go in succession,
 * and remove all but one element from them.
 */
public class TestC {

  public static int[] getSeries(int[] argArray) {
    if (argArray.length <= 1) {
      return argArray;
    }
    int[] _array = new int[argArray.length];
    int iterator = 0;

    _array[0] = argArray[0];
    for (int i = 1; i < argArray.length; i++) {
      if (argArray[i] != _array[iterator]) {
        iterator++;
        _array[iterator] = argArray[i];
      }
    }
    int[] finalArray = new int[iterator + 1];
    System.arraycopy(_array, 0, finalArray, 0, iterator + 1);
    return finalArray;
  }
}
