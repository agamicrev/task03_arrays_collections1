package arrays;

/**
 * Given two arrays. Form a third array consisting of those elements that:
 * a) are present in both arrays; b) present only in one of the arrays.
 */
public class TestA {

  public int[] getResultOfTestA(int[] array1, int[] array2) {
    int[] temp = new int[array1.length + array2.length];
    for (int i = 0, j = 0; i < temp.length; i++) {
      temp[i] = (i < array1.length) ? array1[i] : array2[j++];
    }
    return temp;
  }

  private int[] getUnique(int[] array1, int[] array2) {
    int[] unique = new int[0];
    for (int i = 0, d = 0; i < array1.length; i++) {
      int temp = 0;
      boolean checker = false;
      for (int j = 0; j < array2.length; j++) {
        if (array1[i] == array2[j]) {
          checker = false;
          break;
        } else if (array1[i] != array2[j]) {
          temp = array1[i];
          checker = true;
        }
      }

      if (checker) {
        int[] tmp = unique;
        unique = new int[++d];

        for (int j = 0; j < tmp.length; j++) {
          unique[j] = tmp[j];
        }

        unique[d - 1] = temp;
      }
    }

    return unique;
  }

  public int[] getResultOfTestB(int[] array1, int[] array2) {
    int[] first = new TestA().getUnique(array1, array2);
    int[] second = new TestA().getUnique(array2, array1);

    return getResultOfTestA(first, second);
  }
}
