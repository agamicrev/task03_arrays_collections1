package arrays;

import java.util.Arrays;

/**
 * B. Delete in array all numbers that are repeated more than twice.
 */
public class TestB {

  public int[] removeIfNumberDuplicatesMoreThanTwice(int[] argArray) {
    if (argArray.length <= 1) {
      return argArray;
    }
    int[] array = new int[argArray.length];
    System.arraycopy(argArray, 0, array, 0, array.length);

    // Additional array for storing numbers, duplicated more than twice
    int[] additionalArray = new int[array.length / 3];
    int iterator = 0;

    Arrays.sort(array);

    int repeats = 1;
    for (int i = 1; i < array.length; i++) {
      if (array[i] == array[i - 1]) {
        repeats++;
      } else {
        if (repeats > 2) {
          additionalArray[iterator++] = array[i - 1];
        }
        repeats = 1;
      }
    }

    int[] _array = new int[array.length - 3 * iterator];
    iterator = 0;
    for (int i : argArray) {
      boolean isPresent = false;
      for (int j : additionalArray) {
        if (i == j) {
          isPresent = true;
        }
      }
      if (!isPresent) {
        _array[iterator++] = i;
      }
    }
    int[] finalArray = new int[iterator];
    System.arraycopy(_array, 0, finalArray, 0, iterator);
    return finalArray;
  }
}
