/**
 * D. A hero of a computer game with a force of 25 points, is in a round room,
 * of which there are 10 closed doors. At each door, the hero waits for either
 * a magical artifact that gives a force of 10 to 80 points, or a monster that
 * has a force of 5 to 100 points, with which hero to fight. The battle wins the
 * character that has the greatest power; if the forces are equal, the hero wins.
 * 1. Organize entering information about what is behind the door, or fill it with
 * a random number generator.
 * 2. Display the same information on the screen in a clear tabular form.
 * 3. Calculate how many doors the hero waits for death. Recursive
 * 4. Display the door numbers in the order in which they should be opened by the
 * hero to stay alive, if possible.
 */
package arraysTestD;

import java.util.Arrays;

import static java.util.concurrent.ThreadLocalRandom.current;

public class Main {

  private Entity[] behindTheDoors = null;
  private int doorCount = 10;

  private Hero hero = null;

  private int[] openedDoors = null;

  public Main() {
    behindTheDoors = new Entity[doorCount];
    hero = new Hero(25);
    openedDoors = new int[doorCount];
    Arrays.fill(openedDoors, -1);
  }

  /**
   * Randomly generates by uniform distribution law Monsters and Artifacts.
   */
  public void generateEntities() {
    for (int i = 0; i < behindTheDoors.length; i++) {
      int rnd = current().nextInt();
      if ((rnd & 1) == 0) {
        behindTheDoors[i] = new Monster(current().nextInt(100 - 5) + 5);
      } else {
        behindTheDoors[i] = new Artifact(current().nextInt(11 - 10) + 10);
      }
    }
  }

  /**
   * Prints the data.
   */
  public void print() {
    int i = 1;
    String format = "%-8s\t|\t%-8s\t|\t%d";
    for (Entity door : behindTheDoors) {
      System.out.println(String.format(format,
          "Door #" + i++,
          door.toString(),
          door.getValue()));
    }
  }

  /**
   * Calculates recursively death doors.
   *
   * @param currentDoor which recursive function checks.
   * @param deathDoors  var for storing.
   * @return count of death doors.
   */
  public int deathDoorCount(int currentDoor, Integer deathDoors) {
    if (currentDoor == doorCount) return deathDoors;

    if (behindTheDoors[currentDoor] instanceof Monster &&
        hero.getStrength() < behindTheDoors[currentDoor].getValue()) {
      deathDoors++;
    }
    return deathDoorCount(currentDoor + 1, deathDoors);
  }

  /**
   * Builds a sequence of door numbers.
   */
  public void openAll() {
    int iterator = 0;
    int cycles = 0;
    do {
      cycles++;
      for (int i = 0; i < behindTheDoors.length; i++) {
        boolean isAlreadyOpened = false;
        for (int opened : openedDoors) {
          if (opened == i) {
            isAlreadyOpened = true;
          }
        }
        if (isAlreadyOpened) {
          continue;
        }
        Entity door = behindTheDoors[i];
        if (door instanceof Artifact) {
          hero.setStrength(hero.getStrength() + door.getValue());
          openedDoors[iterator++] = i;
        } else if (door instanceof Monster) {
          if (hero.getStrength() >= door.getValue()) {
            openedDoors[iterator++] = i;
          }
        }
      }
      if (cycles >= doorCount * 2) break;
    } while (iterator != doorCount);
  }

  public static void main(String[] args) {
    Main main = new Main();
    main.generateEntities();
    main.print();
    int deathDoorCount = main.deathDoorCount(0, new Integer(0));
    System.out.println("Stronger monster behind the " + deathDoorCount + " door");
    main.openAll();
    System.out.print("Steps to survive: ");
    if (main.openedDoors[main.doorCount - 1] == -1) {
      System.out.println("You can't survive!");
    } else {
      System.out.println(Arrays.toString(main.openedDoors));
    }
  }
}
