package arraysTestD;

public class Monster implements Entity {

  private int strength;

  public Monster(int strength) {
    this.strength = strength;
  }

  @Override
  public int getValue() {
    return strength;
  }

  @Override
  public String toString() {
    return "Monster";
  }
}
