package shipdroids;

public class BattleDroid extends Droid {

  private String weapon;

  public BattleDroid(String name, int ID, int priority, String weapon) {
    super(name, ID, priority);
    this.weapon = weapon;
  }

  public String getWeapon() {
    return weapon;
  }

  public void setWeapon(String weapon) {
    this.weapon = weapon;
  }

  @Override
  public String toString() {
    return "BattleDroid{" +
        "weapon='" + weapon + '\'' +
        '}';
  }
}
