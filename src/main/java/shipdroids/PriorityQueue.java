package shipdroids;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PriorityQueue<T extends Droid>{

  private Droid[] heap;
  private int heapSize, capacity;

  public PriorityQueue(int capacity) {
    this.capacity = capacity + 1;
    heap = new Droid[this.capacity];
    heapSize = 0;
  }

  public void clear() {
    heap = new Droid[capacity];
    heapSize = 0;
  }

  public boolean isEmpty() {
    return heapSize == 0;
  }

  public boolean isFull() {
    return heapSize == capacity - 1;
  }

  public int size() {
    return heapSize;
  }

  public void insert(String name,int ID, int priority)
  {
    T newJob = (T) new Droid(name,ID , priority);

    heap[++heapSize] = newJob;
    int pos = heapSize;
    while (pos != 1 && newJob.getPriority() > heap[pos/2].getPriority())
    {
      heap[pos] = heap[pos/2];
      pos /=2;
    }
    heap[pos] = newJob;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < heap.length; i++) {
      sb.append(heap[i] + "\n");
    }
    return sb.toString();
  }

  public T remove()
  {
    int parent, child;
    T item, temp;
    if (isEmpty() )
    {
      System.out.println("Heap is empty");
      return null;
    }

    item = (T) heap[1];
    temp = (T) heap[heapSize--];

    parent = 1;
    child = 2;
    while (child <= heapSize)
    {
      if (child < heapSize && heap[child].getPriority() < heap[child + 1].getPriority())
        child++;
      if (temp.getPriority() >= heap[child].getPriority())
        break;

      heap[parent] = heap[child];
      parent = child;
      child *= 2;
    }
    heap[parent] = temp;
    return item;
  }
}