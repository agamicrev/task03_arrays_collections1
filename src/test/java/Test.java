import arrays.TestA;
import arrays.TestB;
import arrays.TestC;

import java.util.Arrays;
import shipdroids.BattleDroid;
import shipdroids.Droid;
import shipdroids.PriorityQueue;
import shipdroids.Ship;

import java.util.ArrayList;
import java.util.List;

public class Test {

  public static void main(String[] args) {
    arrayTest();
    shipdroidsTest();
  }

  private static void arrayTest() {

    // Task A. Intersection(a) and difference(b)
    System.out.println("RESULT ARRAYS");
    int[] array1 = {1, 5, 3, 4, 2, 4, 2};
    int[] array2 = {5, 6, 7, 8, 9, 2, 10, 12};

    System.out.print("We have array #1: [");
    for (int i : array1) {
      System.out.print(i + " ");
    }
    System.out.println("]");

    System.out.print("We have array #2: [");
    for (int i : array2) {
      System.out.print(i + " ");
    }
    System.out.println("]");

    int[] array3 = new TestA().getResultOfTestA(array1, array2);
    System.out.print("Task A (a): [");
    for (int i : array3) {
      System.out.print(i + " ");
    }
    System.out.println("]");
    array3 = new TestA().getResultOfTestB(array1, array2);
    System.out.print("Task A (b): [");
    for (int i : array3) {
      System.out.print(i + " ");
    }
    System.out.println("]");

    // Task B. Array with elements that are repeated up to a maximum of twice
    int[] array4 = {1, 1, 1, 1, 2, 2, 3, 3, 5, 4, 1, 1, 1, 2};
    System.out.println("\nTask B. Array with elements that are repeated up to a maximum of twice\n" +
        "We have array: " + Arrays.toString(array4));
    int[] handledArray = new TestB().removeIfNumberDuplicatesMoreThanTwice(array4);
    System.out.println("If numbers duplicates more than twice:");
    System.out.println(Arrays.toString(handledArray));


    // Task C. Replace series of numbers by one number
    int[] series = new TestC().getSeries(array4);
    System.out.println("\nTask C. Replace series of numbers by one number:");
    System.out.println(Arrays.toString(series));
  }

  private static void shipdroidsTest() {
    System.out.println("\nSHIP WITH DROIDS");
    Ship<? super BattleDroid> ship = new Ship<>();

    ship.add(new Droid("R2D2", 12, 1));
    ship.add(new BattleDroid("R3D3", 2, 1, "sword"));
    ship.add(new Droid("R4D4", 5, 1));
    ship.add(new BattleDroid("R5D5", 67, 1, "blaster"));

    for (int i = 0; i < 4; i++) {
      System.out.println(ship.get(i));
    }

    PriorityQueue<? super BattleDroid> PQ = new PriorityQueue<>(5);

    PQ.insert("d2", 12, 1);
    PQ.insert("d2", 12, 2);
    PQ.insert("d2", 12, 3);
    PQ.insert("d2", 12, 4);

    System.out.println("\nSHIP WITH DROIDS (PRIORITY QUEUE)");
    System.out.println(PQ);

    List<Integer> myList = new ArrayList<>();
    myList.forEach(i -> System.out.println(i));
  }
}
